import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { Box } from "../components/models/box";

@Injectable({
  providedIn: "root",
})
export class BoxService {
  boxes: any = [
    [
      { isMain: true },
      { isMain: false },
      { isMain: false },
      { isMain: false },
      { isMain: false },
      { isMain: false },
      { isMain: false },
    ],
    [
      { isMain: false },
      { isMain: false },
      { isMain: false },
      { isMain: false },
      { isMain: false },
      { isMain: false },
      { isMain: false },
    ],
    [
      { isMain: false },
      { isMain: false },
      { isMain: false },
      { isMain: false },
      { isMain: false },
      { isMain: false },
      { isMain: false },
    ],
    [
      { isMain: false },
      { isMain: false },
      { isMain: false },
      { isMain: false },
      { isMain: false },
      { isMain: false },
      { isMain: false },
    ],
  ];

  constructor(private http: HttpClient) {}

  getBoxes(): Observable<Box[]> {
    return of(this.boxes);
  }
}
