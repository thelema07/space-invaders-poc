import { Component, OnInit, OnDestroy } from "@angular/core";
import { BoxService } from "src/app/services/box.service";
import { Box } from "../models/box";
import { Subscriber } from "rxjs";

@Component({
  selector: "app-container",
  templateUrl: "./container.component.html",
  styleUrls: ["./container.component.scss"],
})
export class ContainerComponent implements OnInit, OnDestroy {
  subscriptions = new Subscriber();
  boxes: Box[] = [];

  constructor(private boxService: BoxService) {}

  ngOnInit() {
    const boxSubscription = this.boxService
      .getBoxes()
      .subscribe((response: Box[]) => {
        this.boxes = response;
      });

    this.subscriptions.add(boxSubscription);
  }

  setBox(indexCol, indexRow) {
    this.boxes.forEach((boxset: any, boxSetIndex) => {
      boxset.forEach((box, boxIndex) => {
        if (boxSetIndex === indexRow && indexCol === boxIndex) {
          box.isMain = true;
        }
      });
    });
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
